<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Palmate Task(Deeptonabho Dutta)</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    </head>
    <body>
     <div class="container mt-0">
            <table class="table table-bordered mb-5">
                <thead>
                    <tr class="table-success">
                        <th scope="col">First name</th>
                        <th scope="col">Last name</th>
                        <th scope="col">Email</th>
                    </tr>
                </thead>
                <tbody>
                    
        @for($i=1;$i<$values[1];$i++)
        <tr>
            @for($j=0;$j<$values[2];$j++)
                @isset($values[0][$i][$j])
                    <td>{{$values[0][$i][$j]}}</td>
                @endisset
            @endfor
            <br>
            @endfor
                    </tr>
                </tbody>
            </table>

    </body>
</html>
